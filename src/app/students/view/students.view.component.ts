import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from '../../service/student-service';
import Student from '../../entity/student';

@Component({
  selector: 'app-students-view',
  templateUrl: './students.view.component.html',
  styleUrls: ['./students.view.component.css']
})

export class StudentsViewComponent implements OnInit {
  students: Student[];
  student: Student;
  constructor(private route: ActivatedRoute, private studentService: StudentService) { }
  ngOnInit():void {
	  this.route.params
	  .subscribe((params: Params) => {
		  this.studentService.getStudent(params['id'])
		  .subscribe((inputStudent: Student) => this.student = inputStudent);
    });
  }
}
